FROM condaforge/mambaforge as mamba

COPY environment.yml /environment.yml
RUN mamba env create -p /env -f environment.yml \
  && conda clean -afy

FROM debian:11-slim

# curl is used by the livenessProbe on Kubernetes deployment
RUN apt-get update \
  && apt-get install -y curl \
  && rm -rf /var/lib/apt/lists/*

EXPOSE 5004

COPY --from=mamba /env /env

WORKDIR /tangogql
COPY . .

ENV PYTHONUNBUFFERED 1
ENV PYTANGO_GREEN_MODE asyncio

ENV PATH /env/bin:$PATH

RUN chmod 777 /tangogql \
  && useradd -m tango
USER tango

CMD python -m tangogql
