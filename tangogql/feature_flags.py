"""
Read a configuration file and set feature flags for code under development
This was a new feature introduced so SKA could switch off the subscription
to Tango events while it was still being refined
"""
from configparser import ConfigParser
import logging as logger


class FeatureFlags:
    """
    Reads a configuration file to set feature flags for code under development
    can optionally be injected with a set of flags for ease of testing

    :type feature_flags: dictionary of flags (True = On, False = Off)

    """

    def __init__(self, feature_flags=None):
        """
        :type feature_flags: dictionary of flags
        """
        logger.info(f"Initialising  feature flags as : {feature_flags}")
        self._feature_flags = feature_flags

    def read_configs(self):
        """
        Flags are lazily instantiated the first time they are read
        currently read from the file tangogql.ini. If the file can
        not be found then a default set is used.
        """

        default = {"publish_subscribe": False}
        try:
            parser = ConfigParser()
            parser.read('./tangogql/tangogql.ini')
            # default is false
            self._feature_flags = dict(parser['feature_flags'])
        except Exception as err:  # pylint: disable=broad-except
            logger.info(f"Problem reading configuration file: {err}")
            self._feature_flags = default
        return self._feature_flags

    # getter method
    def get_flag(self, key):
        """return a boolean representing the flag - handle initial parse as strings"""
        logger.debug("Entered get flag")
        if self._feature_flags is None:
            self._feature_flags = self.read_configs()
        result = self._feature_flags[key]
        if isinstance(result, bool):
            return result
        return result == "True"

    # setter method
    def set_flag(self, key, value):
        """set the flag - handles being passed either a string or a boolean"""
        if self._feature_flags is None:
            self._feature_flags = self.read_configs()
        if isinstance(value, bool):
            self._feature_flags[key] = value
        else:
            self._feature_flags[key] = (value == "True")
