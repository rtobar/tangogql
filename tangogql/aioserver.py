#!/usr/bin/env python3

"""A simple http backend for communicating with a TANGO control system

The idea is that each client establishes a websocket connection with
this server (on /socket), and sets up a number of subscriptions to
TANGO attributes.  The server keeps track of changes to these
attributes and sends events to the interested clients. The server uses
Taurus for this, so polling, sharing listeners, etc is handled "under
the hood".

There is also a GraphQL endpoint (/db) for querying the TANGO database.
"""
import logging as logger
import logging.config
import aiohttp
import aiohttp_cors
import asyncio
import uuid
import os
import json
import sys
import yaml
from .version import __version__

__all__ = ['run']


def ensure_connectivity(retries=5, sleep_duration=5):
    """
    Attempt to connect to the TANGO host specified by the TANGO_HOST
    environment variable by instantiating a PyTango.Database object. Upon
    failure it will retry up to `retries` times, sleeping for `sleep_duration`
    seconds between attempts. If no connection can be established even after
    retrying, it will cause the program to exit with code 1. Progress is
    reported to stdout as follows:

    (1/5) Trying to connect to tango-host:10000... Failed! Retrying in 5 seconds.
    (2/5) Trying to connect to tango-host:10000... Failed! Retrying in 5 seconds.
    (3/5) Trying to connect to tango-host:10000... Connected!

    :param retries: The number of retries before exiting
    :param sleep_duration: The number of seconds to sleep between attempts.
    :returns: None
    """

    import PyTango, time
    host = None

    for retry in range(1, retries+1):
        try:
            host = f"{PyTango.Database().get_db_host()}:{PyTango.Database().get_db_port()}"
            logger.info(f"({retry}/{retries}) Trying to connect to {host} ...")
            PyTango.Database()
        except PyTango.ConnectionFailed:
            logger.warning(f"Failed to connect to {host}...")
            if retry == retries:
                logger.critical(f"Finished failed trying to connect to {host}...")
                sys.exit(1)
            else:
                logger.info(f"Retrying to connect to {host} in {sleep_duration} seconds.")
                time.sleep(sleep_duration)
        else:
            logger.info(f"Connected to connect to {host}!")
            break

# A factory function is needed to use aiohttp-devtools for live reload functionality.
def setup_server():
    ensure_connectivity()

    from tangogql.routes import routes
    from tangogql.config import Config

    app = aiohttp.web.Application(debug=True)

    config = Config(open("config.json"))
    app["config"] = config

    defaults_dict = {"*": aiohttp_cors.ResourceOptions(
                                            allow_credentials=True,
                                            expose_headers="*",
                                            allow_headers="*")
                     }

    cors = aiohttp_cors.setup(app, defaults=defaults_dict)
    app.router.add_routes(routes)
    for r in list(app.router.routes()):
        cors.add(r)
    app.router.add_static('/', 'static')

    return app


def setup_logger(default_path='logging.yaml',
                 default_level=logging.DEBUG,
                 env_key='LOG_CFG'):
    """Setup logging configuration

    """
    logger = logging.getLogger(__name__)

    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = yaml.safe_load(f.read())
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

    return logger

def setup():
    return (
        setup_logger(),
        setup_server()
    )

# Called by aiohttp-devtools when restarting the dev server.
# Not used in production
def dev_run():
    (app, _) = setup()
    return app

def run():
    (logger, app) = setup()

    loop = asyncio.get_event_loop()
    handler = app.make_handler(debug=True)

    host = os.getenv("TANGOGQL_HOST", "0.0.0.0")
    port = int(os.getenv("TANGOGQL_PORT", "5004"))

    f = loop.create_server(handler, host, port)

    logger.info(f"Running tangogql version: {__version__}")
    logger.info(f"Point your browser to {host}:{port}/graphiql")
    srv = loop.run_until_complete(f)
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        print("Ctrl-C was pressed")
    finally:
        loop.run_until_complete(handler.shutdown())
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(app.cleanup())
    loop.close()

if __name__ == "__main__":
    run()
