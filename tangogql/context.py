"""module that defines the user context"""
import jwt


__all__ = ["build_context"]


class ClientInfo:  # pylint: disable=too-few-public-methods
    """Details of the current user and their associated groups"""
    def __init__(self, user, groups):
        if user is not None and not isinstance(user, str):
            raise TypeError("user must be a string or None")

        if not isinstance(groups, list):
            raise TypeError("groups must be a list")

        self.user = user
        self.groups = groups


def build_context(request, config):
    """Create the user context object based on the Java Web Token in the request"""
    if hasattr(config, 'no_auth'):
        return {
            "config": config
        }
    try:
        token = request.cookies.get("taranta_jwt", "")
        claims = jwt.decode(token, config.secret, algorithms="HS256")
    except jwt.InvalidTokenError:
        claims = {}

    user = claims.get("username")
    groups = claims.get("groups", [])

    return {
        "client": ClientInfo(user, groups),
        "config": config
    }
