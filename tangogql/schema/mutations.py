"""Module containing the available mutations."""

import logging as logger
import asyncio

from datetime import datetime
from tango import DevFailed, ConnectionFailed, CommunicationFailed, DeviceUnlocked

from graphene import ObjectType, Mutation, String, Boolean, List, Field
from tangogql.schema.base import db, proxies
from tangogql.schema.types import ScalarTypes
from tangogql.schema.attribute import DeviceAttribute, collaborative_read_attribute
from tangogql.auth import authorization, authentication

def get_username_from_info(info):
    """extract the name from the client info"""
    if hasattr(info.context["config"], 'no_auth'):
        return "noauth"
    return info.context['client'].user


class ExecuteDeviceCommand(Mutation):
    """This class represent a mutation for executing a command."""

    class Arguments:  # pylint: disable=too-few-public-methods
        """arguments relating to the execute device command"""
        device = String(required=True)
        command = String(required=True)
        argin = ScalarTypes()

    ok = Boolean()
    message = List(String)
    output = ScalarTypes()

    @authentication
    @authorization
    async def mutate(self, info, device, command, argin=None):   # pylint: disable=no-self-use
        """ This method executes a command.

        :param info : User information containing the name of the user making the request
        :param device: Name of the device that the command will be executed.
        :type device: str

        :param command: Name of the command
        :type command: str

        :param argin: The input argument for the command
        :type argin: str or int or bool or float

        :return: Return ok = True and message = Success
                 if the command executes successfully, False otherwise.
                 When an input is not one of the scalar types or an exception
                 has been raised while executing the command, it returns
                 message = error_message.
        :rtype: ExecuteDeviceCommand
        """
        user = get_username_from_info(info)
        logger.info(
            f"MUTATION - ExecuteDeviceCommand - Self: {self},  "
            f"User: {user}, "
            f"Device: {device}, "
            f"Command: {command}, "
            f"Argin: {argin}")

        if isinstance(argin, ValueError):
            return ExecuteDeviceCommand(ok=False, message=[str(argin)])
        try:
            proxy = proxies.get(device)
            result = await proxy.command_inout(command, argin)
            logger.debug(f"{device} :: Execute command result: {result}")
            return ExecuteDeviceCommand(ok=True,
                                        message=["Success"],
                                        output=result)
        except (DevFailed, ConnectionFailed,
                CommunicationFailed, DeviceUnlocked) as error:
            logger.error(f"{device} :: Exception type  {type(error)} in ExecuteDeviceCommand")
            logger.debug(f"{error}")
            err = error.args[0]
            return ExecuteDeviceCommand(ok=False, message=[err.desc, err.reason])
        except Exception as err:  # pylint: disable=broad-except# pylint: disable=broad-except
            logger.error(f"{device} :: Exception in ExecuteDeviceCommand")
            logger.debug(f"{str(err)}")
            return ExecuteDeviceCommand(ok=False, message=[str(err)])


class SetAttributeValue(Mutation):
    """This class represents the mutation for setting value to an attribute."""

    class Arguments:  # pylint: disable=too-few-public-methods
        """The expected arguments for setting value to an attribute."""
        device = String(required=True)
        name = String(required=True)
        value = ScalarTypes(required=True)

    ok = Boolean()
    message = List(String)
    value_before = ScalarTypes()
    attribute = Field(DeviceAttribute)

    @authentication
    @authorization
    async def mutate(self, info, device, name, value):
        """ This method sets value to an attribute.

        :param info : User information containing the name of the user making the request
        :param device: Name of the device
        :type device: str

        :param name: Name of the attribute
        :type name: str
        :param value: The value to be set
        :type value: int, str, bool or float

        :return: Return ok = True and message = Success if successful,
                 False otherwise.
                 When an input is not one the scalar types or an exception has
                 been raised while setting the value returns
                 message = error_message.
        :rtype: SetAttributeValue
        """

        user = get_username_from_info(info)
        logger.info(
            f"MUTATION - SetAttributeValue - User: {user}, Device: {device}, "
            f"Attribute: {name}, Value: {value}")
        if isinstance(value, ValueError):
            return SetAttributeValue(ok=False, message=[str(value)], attribute=None)
        try:
            proxy = proxies.get(device)
            before = await collaborative_read_attribute(proxy, name)
            read_coro = proxy.write_read_attribute(name, value)
            read_fut = asyncio.ensure_future(read_coro)
            result = await read_fut
            before_value = getattr(before, 'value', None)
            result_value = getattr(result, 'value', None)
            logger.debug(f"{device} :: SetAttributeValue - Value: {value}, "
                         f"Value before: {before_value}, Value after: {result_value}")

            return SetAttributeValue(ok=True, message=["Success"],value_before=before.value, attribute=DeviceAttribute(
                name=name,
                device=device,
                _attr_read=read_fut
            ))

        except (DevFailed, ConnectionFailed,
                CommunicationFailed, DeviceUnlocked) as error:
            logger.error(f"{device} :: Exception type {type(error)} in SetAttributeValue")
            logger.debug(f"{error}")
            err = error.args[0]
            return SetAttributeValue(ok=False, message=[err.desc, err.reason], attribute=None)
        except Exception as err:  # pylint: disable=broad-except
            logger.error(f"{device} :: Exception in SetAttributeValue")
            logger.debug(f"{str(err)}")
            return SetAttributeValue(ok=False, message=[str(err)], attribute=None)


class PutDeviceProperty(Mutation):
    """This class represents mutation for putting a device property."""

    class Arguments:  # pylint: disable=too-few-public-methods
        """arguments relating to adding a device property"""
        device = String(required=True)
        name = String(required=True)
        value = List(String)
        # async = Boolean()

    ok = Boolean()
    message = List(String)

    @authentication
    @authorization
    def mutate(self, info, device, name, value=""):  # pylint: disable=no-self-use
        """ This method adds property to a device.

        :param info: User details - expected to contain a name attribute
        :param device: Name of a device
        :type device: str
        :param name: Name of the property
        :type name: str
        :param value: Value of the property
        :type value: str

        :return: Returns ok = True and message = Success if successful,
                 False otherwise.
                 If an exception has been raised returns
                 message = error_message.
        :rtype: PutDeviceProperty
        """
        user = get_username_from_info(info)
        logger.info(f"MUTATION - PutDeviceProperty - User: {user}, "
                    f"Device: {device}, Name: {name}, Value: {value}")
        # wait = not args.get("async")
        try:

            db.put_device_property(device, {name: value})
            return PutDeviceProperty(ok=True, message=["Success"])
        except (DevFailed, ConnectionFailed, CommunicationFailed, DeviceUnlocked) as error:
            logger.error(f"{device} :: Exception type {type(error)} in PutDeviceProperty")
            logger.debug(f"{error}", error)
            err = error.args[0]
            return PutDeviceProperty(ok=False, message=[err.desc, err.reason])
        except Exception as err:  # pylint: disable=broad-except
            logger.error(f"{device} :: Exception in PutDeviceProperty")
            logger.debug(f"{str(err)}")
            return PutDeviceProperty(ok=False, message=[str(err)])


class DeleteDeviceProperty(Mutation):  # pylint: disable=too-few-public-methods
    """This class represents mutation for deleting property of a device."""

    class Arguments:  # pylint: disable=too-few-public-methods
        """arguments expected for deleting property of a device.
        - both device and name are  mandatory"""
        device = String(required=True)
        name = String(required=True)

    ok = Boolean()
    message = List(String)

    @authentication
    @authorization
    def mutate(self, info, device, name):  # pylint: disable=no-self-use
        """This method delete a property of a device.

        :param info: User details - expected to contain a name attribute
        :param device: Name of the device
        :type device: str
        :param name: Name of the property
        :type name: str

        :return: Returns ok = True and message = Success if successful,
                 ok = False otherwise.
                 If exception has been raised returns message = error_message.
        :rtype: DeleteDeviceProperty
        """
        user = get_username_from_info(info)
        logger.info(f"MUTATION - DeleteDeviceProperty - User: {user}, "
                    f"Device: {device}, Name: {name}")
        try:
            db.delete_device_property(device, name)
            return DeleteDeviceProperty(ok=True, message=["Success"])
        except (DevFailed, ConnectionFailed,
                CommunicationFailed, DeviceUnlocked) as error:
            logger.error(f"{device} :: Exception type {type(error)} in DeleteDeviceProperty")
            logger.debug(f"{error}")
            err = error.args[0]
            return DeleteDeviceProperty(ok=False, message=[err.desc, err.reason])

        except Exception as err:  # pylint: disable=broad-except
            logger.error(f"{device} :: Exception in DeleteDeviceProperty")
            logger.debug(f"{str(err)}")
            return DeleteDeviceProperty(ok=False, message=[str(err)])


class Mutations(ObjectType):  # pylint: disable=too-few-public-methods
    """This class contains all the mutations."""

    put_device_property = PutDeviceProperty.Field()
    delete_device_property = DeleteDeviceProperty.Field()
    setAttributeValue = SetAttributeValue.Field()
    execute_command = ExecuteDeviceCommand.Field()
